// this command just loads whatever constant you want into the D register
@17
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@17
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMb
D;JEQ // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMc
0;JMP
(SYMb)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMc)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@17
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@16
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMd
D;JEQ // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMe
0;JMP
(SYMd)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMe)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@16
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@17
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMf
D;JEQ // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMg
0;JMP
(SYMf)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMg)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@892
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@891
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMh
D;JLT // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMi
0;JMP
(SYMh)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMi)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@891
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@892
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMj
D;JLT // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMk
0;JMP
(SYMj)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMk)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@891
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@891
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMl
D;JLT // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMm
0;JMP
(SYMl)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMm)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@32767
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@32766
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMn
D;JGT // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMo
0;JMP
(SYMn)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMo)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@32766
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@32767
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMp
D;JGT // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMq
0;JMP
(SYMp)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMq)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@32766
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@32766
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the value of first element and store in D
A=A-1 // reset A to next element in stack
D=M-D
// control flow to check for equivalences etc
@SYMr
D;JGT // subtract first element from the second and jump (or don't) based on that
@SP
A=M-1
A=A-1
M=0 // if here then they did not meet the specified condition, set second element to false
@SYMs
0;JMP
(SYMr)
// if here then they did meet the specified condition, set second element to true
@SP
A=M-1
A=A-1
M=-1
(SYMs)
@SP
A=M-1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@57
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@31
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@53
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=D+M
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@112
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=M-D
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive adr of first element from adr of sp and load it into A
D=-M
M=D // replace the old value with the negated value
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=D&M
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@82
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=D|M
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point a to mem adr 0
A=M-1 // derive adr of first element from adr of sp and load it into A
D=!M
M=D // replace the old value with the negated value
D=0 // zero out the registers to prevent cross command contamination
A=0
