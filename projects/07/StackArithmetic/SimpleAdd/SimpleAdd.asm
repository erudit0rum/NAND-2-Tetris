// this command just loads whatever constant you want into the D register
@7
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@8
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=D+M
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
