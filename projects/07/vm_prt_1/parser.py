class Parser:
    # some of these commands can probably be snarfed
    def __init__(self, file_name, regex_obj):
        print 'initializing parser module'

        print 'attaching the regex object to the parser'
        self.__regex = regex_obj

        print 'opening file'
        with open(file_name) as f:
            self.__program = f.readlines()
        f.close();

        print 'setting current line to 0'
        self.__current_line = 0

        print 'this is the program'
        print self.__program
        print 'stripping out comments, white space, and empty lines'
        while self.__current_line < len(self.__program):

            # if line is blank or a comment then strip it out
            comment_loc = self.__program[self.__current_line].find('//')
            if comment_loc == 0 or \
            self.__program[self.__current_line] == '' or \
            self.__program[self.__current_line].isspace():
                del self.__program[self.__current_line]
                continue

            if comment_loc > 0:
                self.__program[self.__current_line] = \
                self.__program[self.__current_line].split('//')[0]

            self.__program[self.__current_line] = \
            self.__program[self.__current_line].replace('\n', '').replace('\r', '')

            # need some logic here to strip out any spaces at the beggining or the end

            self.__current_line += 1

        print 'this is the program after white space stripped out'
        print self.__program
        print 'resetting the current line to 0'
        self.__current_line = 0

    def has_more_commands(self):
        return self.__current_line < len(self.__program)

    def advance(self):
        self.__current_line += 1

    def command_type(self):
        if self.__regex.search('(^add\Z|^sub\Z|^neg\Z|^eq\Z|^gt\Z|^lt\Z|^and\Z|^or\Z|^not\Z)', self.current_command()):
            return 'C_ARITHMETIC'
        elif self.__regex.search('^push\s[^\s]+\s[^\s]+\Z', self.current_command()):
            return 'C_PUSH'
        elif self.__regex.search('^pop\s[^\s]+\s[^\s]+\Z', self.current_command()):
            return 'C_POP'
        elif self.__regex.search('^label\s[^\s]+\Z', self.current_command()):
            return 'C_LABEL'
        elif self.__regex.search('^goto\s[^\s]+\Z', self.current_command()):
            return 'C_GOTO'
        elif self.__regex.search('^if-goto\s[^\s]+\Z', self.current_command()):
            return 'C_IF'
        elif self.__regex.search('^push\s[^\s]+\s[^\s]+\Z', self.current_command()):
            return 'C_FUNCTION'
        elif self.__regex.search('^return\Z', self.current_command()):
            return 'C_RETURN'
        elif self.__regex.search('^push\s[^\s]+\s[^\s]+\Z', self.current_command()):
            return 'C_CALL'
        else:
            raise Exception()

    def arg1(self):
        command_type = self.command_type()
        if command_type == 'C_RETURN':
            raise Exception('should not fire now')
        elif command_type == 'C_ARITHMETIC':
            return self.current_command()
        else:
            return self.current_command().split(' ')[1]

    def arg2(self):
        command_type = self.command_type()
        allowed_command_types = [
            'C_PUSH', 'C_POP', 'C_FUNCTION', 'C_CALL'
        ]
        if command_type in allowed_command_types:
            return self.current_command().split(' ')[2]
        else:
            raise Exception('should not fire now')

    def current_command(self):
        return self.__program[self.__current_line]

    def current_base_command(self):
        return self.__program[self.__current_line].split(' ')[0]

    def current_line(self):
        return self.__current_line

    def reset_current_line(self):
        self.__current_line = 0

    def delete_current_line(self):
        del self.__program[self.__current_line]


