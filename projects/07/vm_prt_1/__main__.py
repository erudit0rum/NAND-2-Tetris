# packages
import re
import yaml

# classes
from parser import Parser
from code_writer import Code

# input and output files
#input_file_path = '../MemoryAccess/PointerTest/PointerTest.vm'
#output_file_path = 'PointerTest.asm'

#input_file_path = '../MemoryAccess/BasicTest/BasicTest.vm'
#output_file_path = 'BasicTest.asm'

#input_file_path = '../MemoryAccess/StaticTest/StaticTest.vm'
#output_file_path = 'StaticTest.asm'

#input_file_path = '../StackArithmetic/SimpleAdd/SimpleAdd.vm'
#output_file_path = 'SimpleAdd.asm'

input_file_path = '../StackArithmetic/StackTest/StackTest.vm'
output_file_path = 'StackTest.asm'

# config files
stack_command_path = 'stack_commands.yaml'
push_command_path = 'push_commands.yaml'
pop_command_path = 'pop_commands.yaml'

# instantiate the classes and pass them their dependencies
prs = Parser(input_file_path, re)
cdwrt = Code(
    output_file_path, stack_command_path,
    push_command_path, pop_command_path,
    yaml
)

def sub_routine():
    cdwrt.set_file_name('Test')
    while prs.has_more_commands():
        com_type = prs.command_type()
        base_command = prs.current_base_command()
        if com_type == 'C_POP' or com_type == 'C_PUSH':
            arg1 = prs.arg1()
            arg2 = prs.arg2()
            cdwrt.write_push_pop(base_command, arg1, arg2)
        elif com_type == 'C_ARITHMETIC':
            cdwrt.write_arithmetic(base_command)
        else:
            print 'not equipped to handle this'
        prs.advance()
    cdwrt.close()

def another_sub():
    print 'testing'

def main():
    sub_routine()
    another_sub()

main()
