class Code:
    def __init__(self, output_path, stack_command_path, push_command_path, pop_command_path, yaml_obj):
        print 'initializing code module'

        print 'preparing output file for writing'
        self.__output_file = open(output_path, 'a+')

        print 'reading in prewritten assembler commands'
        self.__stack_commands = yaml_obj.safe_load(open(stack_command_path))
        self.__push_commands = yaml_obj.safe_load(open(push_command_path))
        self.__pop_commands = yaml_obj.safe_load(open(pop_command_path))

        print 'preparing unique symbol generator'
        # when you get a chance move this to a yaml file as well
        self.__internal_symbol_counter = 'SYMa'
        self.__internal_symbol_table = [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z', '_', '.', '$', ':'
        ]
        self.__file_name = ''

    def __get_unique_symbol(self):
        # the purpose of this method and associated variables is to have a
        # reliable way of generating unique symbols for use in programatically
        # generated loops
        right_most_char = self.__internal_symbol_counter[-1:]
        if right_most_char == ':':
            self.__internal_symbol_counter = self.__internal_symbol_counter + 'a'
            return self.__internal_symbol_counter
        else:
            right_most_char_index = self.__internal_symbol_table.index(right_most_char)
            new_right_most_char = self.__internal_symbol_table[right_most_char_index + 1]
            self.__internal_symbol_counter = self.__internal_symbol_counter[:-1] + new_right_most_char
            return self.__internal_symbol_counter

    def __get_file_name(self):
        return self.__file_name

    def set_file_name(self, file_name):
        self.__file_name = file_name

    def write_arithmetic(self, command):
        add_sub_and_or_base_command = self.__stack_commands['add_sub_and_or_base_command']
        eq_gt_lt_base_command = self.__stack_commands['eq_gt_lt_base_command']
        neg_not_base_command = self.__stack_commands['neg_not_base_command']

        # binary commands
        if command == "add":
            self.__handle_add(add_sub_and_or_base_command)
        elif command == "sub":
            self.__handle_sub(add_sub_and_or_base_command)
        elif command == "and":
            self.__handle_and(add_sub_and_or_base_command)
        elif command == "or":
            self.__handle_or(add_sub_and_or_base_command)

        # comparative commands
        elif command == "eq":
            self.__handle_eq(eq_gt_lt_base_command)
        elif command == "gt":
            self.__handle_gt(eq_gt_lt_base_command)
        elif command == "lt":
            self.__handle_lt(eq_gt_lt_base_command)

        # unary commands
        elif command == "neg":
            self.__handle_neg(neg_not_base_command)
        elif command == "not":
            self.__handle_not(neg_not_base_command)

        # for any other commands throw an error
        else:
            raise Exception("not recognized command")

    # binary commands
    def __handle_add(self, base_command):
        new_add_command = base_command.format('M=D+M')
        self.__output_file.write(new_add_command)

    def __handle_sub(self, base_command):
        new_sub_command = base_command.format('M=M-D')
        self.__output_file.write(new_sub_command)

    def __handle_and(self, base_command):
        new_and_command = base_command.format('M=D&M')
        self.__output_file.write(new_and_command)

    def __handle_or(self, base_command):
        new_or_command = base_command.format('M=D|M')
        self.__output_file.write(new_or_command)

    # comparative commands
    def __handle_eq(self, base_command):
        new_eq_command = base_command.format('JEQ', self.__get_unique_symbol(), self.__get_unique_symbol())
        self.__output_file.write(new_eq_command)

    def __handle_gt(self, base_command):
        new_gt_command = base_command.format('JGT', self.__get_unique_symbol(), self.__get_unique_symbol())
        self.__output_file.write(new_gt_command)

    def __handle_lt(self, base_command):
        new_lt_command = base_command.format('JLT', self.__get_unique_symbol(), self.__get_unique_symbol())
        self.__output_file.write(new_lt_command)

    # unary commands
    def __handle_neg(self, base_command):
        new_neg_command = base_command.format('D=-M')
        self.__output_file.write(new_neg_command)

    def __handle_not(self, base_command):
        new_not_command = base_command.format('D=!M')
        self.__output_file.write(new_not_command)

    def write_push_pop(self, command, segment, index):
        # for this one, just like the last one we will dispatch to sub
        # commands, unlike the last one, for this one we will have to read memory locations of
        # certain segments from a configuration file

        # sort of similar to the last one we will need to write base commands for these, for the push one
        # we will assume that there is a first half of the command that will deposit the value to be pushed
        # into the D register and then we can dump what is in the D register into the top position in the stack and
        # reset the stack pointer, for the other one we can write the logic to pull the first element off the front of the
        # stack, store it in the D register and then clean up the front of the stack and leave it available for the rest of the
        # command which will dump the rest of the 

        if command == "push":
            self.__handle_push(segment, int(index))
        elif command == "pop":
            self.__handle_pop(segment, int(index))
        else:
            raise Exception('not a valid push or pop command')


    def __handle_push(self, segment, index):
        push_base_command = self.__push_commands['push_base_command']
        adr_fixed_RAM_command = push_base_command.format(self.__push_commands['adr_fixed_RAM_command'][:-1])
        fixed_RAM_command = push_base_command.format(self.__push_commands['fixed_RAM_command'][:-1])
        tot_vir_command = push_base_command.format(self.__push_commands['tot_vir_command'][:-1])
        prog_gen_sym_command = push_base_command.format(self.__push_commands['prog_gen_sym_command'][:-1])

        # location stored on fixed area of RAM
        if segment == 'local':
            self.__handle_push_local(adr_fixed_RAM_command, index)
        elif segment == 'argument':
            self.__handle_push_argument(adr_fixed_RAM_command, index)
        elif segment == 'this':
            self.__handle_push_this(adr_fixed_RAM_command, index)
        elif segment == 'that':
            self.__handle_push_that(adr_fixed_RAM_command, index)

        # mapped directly onto fixed area of RAM
        elif segment == 'pointer':
            if not (index == 0 or index == 1):
                print index
                raise Exception('out of range for pointer')
            else:
                self.__handle_push_pointer(fixed_RAM_command, index)
        elif segment == 'temp':
            if not index >= 5 and index <= 12:
                raise Exception('out of range for temp')
            else:
                self.__handle_push_temp(fixed_RAM_command, index)

        # totally virtual
        elif segment == 'constant':
            self.__handle_push_constant(tot_vir_command, index)

        # programatically generated symbols
        elif segment == 'static':
            self.__handle_push_static(prog_gen_sym_command, index)
        else:
            raise Exception('not a valid segment')


    # location stored on fixed area of RAM
    def __handle_push_local(self, base_command, index):
        new_command = base_command.format('LCL', index)
        self.__output_file.write(new_command)

    def __handle_push_argument(self, base_command, index):
        new_command = base_command.format('ARG', index)
        self.__output_file.write(new_command)

    def __handle_push_this(self, base_command, index):
        new_command = base_command.format('THIS', index)
        self.__output_file.write(new_command)

    def __handle_push_that(self, base_command, index):
        new_command = base_command.format('THAT', index)
        self.__output_file.write(new_command)

    # mapped directly onto fixed area of RAM
    def __handle_push_pointer(self, base_command, index):
        # the pointer base will always be located at 3, thus we always
        # need to point at 3 plus the index
        RAM_location = str(3 + index)

        new_command = base_command.format('pointer', RAM_location)
        self.__output_file.write(new_command)

    def __handle_push_temp(self, base_command, index):
        # the temp base will always be located at 5, thus we always
        # need to point at 5 plus the index
        RAM_location = str(5 + index)

        new_command = base_command.format('temp', RAM_location)
        self.__output_file.write(new_command)

    # totally virtual
    def __handle_push_constant(self, base_command, index):
        new_command = base_command.format(index)
        self.__output_file.write(new_command)

    # programatically generated symbols
    def __handle_push_static(self, base_command, index):
        static_sym = self.__get_file_name() + '.' + str(index)
        new_command = base_command.format(static_sym)
        self.__output_file.write(new_command)

    def __handle_pop(self, segment, index):
        pop_base_command = self.__pop_commands['pop_base_command']
        adr_fixed_RAM_command = pop_base_command.format(self.__pop_commands['adr_fixed_RAM_command'][:-1])
        fixed_RAM_command = pop_base_command.format(self.__pop_commands['fixed_RAM_command'][:-1])
        prog_gen_sym_command = pop_base_command.format(self.__pop_commands['prog_gen_sym_command'][:-1])

        # address is stored in a fixed location in the RAM
        if segment == 'local':
            self.__handle_pop_local(adr_fixed_RAM_command, index)
        elif segment == 'argument':
            self.__handle_pop_argument(adr_fixed_RAM_command, index)
        elif segment == 'this':
            self.__handle_pop_this(adr_fixed_RAM_command, index)
        elif segment == 'that':
            self.__handle_pop_that(adr_fixed_RAM_command, index)

        # data stored directly in one fixed RAM location
        elif segment == 'pointer':
            if not (index == 0 or index == 1):
                print type(index)
                raise Exception('out of range for temp')
            else:
                self.__handle_pop_pointer(fixed_RAM_command, index)
        elif segment == 'temp':
            if not index >= 5 and index <= 12:
                print type(index)
                raise Exception('out of range for temp')
            else:
                self.__handle_pop_temp(fixed_RAM_command, index)

        #totally virtual command
        elif segment == 'constant':
            raise Exception('constants may not be reassigned')

        #programatic symbol generation
        elif segment == 'static':
            self.__handle_pop_static(prog_gen_sym_command, index)

        else:
            raise Exception('not a valid segment')

    # address is stored in a fixed location in the RAM for these commands
    def __handle_pop_local(self, base_command, index):
        new_command = base_command.format('LCL', index)
        self.__output_file.write(new_command)

    def __handle_pop_argument(self, base_command, index):
        new_command = base_command.format('ARG', index)
        self.__output_file.write(new_command)

    def __handle_pop_this(self, base_command, index):
        new_command = base_command.format('THIS', index)
        self.__output_file.write(new_command)

    def __handle_pop_that(self, base_command, index):
        new_command = base_command.format('THAT', index)
        self.__output_file.write(new_command)

    # data stored directly in one fixed RAM location
    def __handle_pop_pointer(self, base_command, index):
        # the pointer base will always be located at 3, thus we always
        # need to point at 3 plus the index
        RAM_location = str(3 + int(index))

        new_command = base_command.format('pointer', RAM_location)
        self.__output_file.write(new_command)

    def __handle_pop_temp(self, base_command, index):
        # the pointer base will always be located at 3, thus we always
        # need to point at that 3 plus the index
        RAM_location = str(5 + index)
        new_command = base_command.format('temp', RAM_location)
        self.__output_file.write(new_command)

    # commands that require the programmatic generation of commands
    def __handle_pop_static(self, base_command, index):
        static_sym = self.__get_file_name() + '.' + str(index)
        new_command = base_command.format(static_sym)
        self.__output_file.write(new_command)

    def close(self):
        self.__output_file.close()

