// this command just loads whatever constant you want into the D register
@10
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@R13
M=D // currently D contains the pop val but we need that register so dump D out to R13 which is unused
@LCL // point A at LCL
D=M // dump the base value into D
@0 // put the index in A
D=D+A // add the index and base and dump the result in D
@R14
M=D // dump the computed address to R14 which is unused
@R13
D=M // pull the pop val back out of R13 and dump in D
@R14
A=M // pull the computed address back out of R14 and point to it with the pop val now in D
M=D // dump the value currently in the D register into the specified location
// no need to reset A, the base command will take care of that
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@21
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@22
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@R13
M=D // currently D contains the pop val but we need that register so dump D out to R13 which is unused
@ARG // point A at ARG
D=M // dump the base value into D
@2 // put the index in A
D=D+A // add the index and base and dump the result in D
@R14
M=D // dump the computed address to R14 which is unused
@R13
D=M // pull the pop val back out of R13 and dump in D
@R14
A=M // pull the computed address back out of R14 and point to it with the pop val now in D
M=D // dump the value currently in the D register into the specified location
// no need to reset A, the base command will take care of that
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@R13
M=D // currently D contains the pop val but we need that register so dump D out to R13 which is unused
@ARG // point A at ARG
D=M // dump the base value into D
@1 // put the index in A
D=D+A // add the index and base and dump the result in D
@R14
M=D // dump the computed address to R14 which is unused
@R13
D=M // pull the pop val back out of R13 and dump in D
@R14
A=M // pull the computed address back out of R14 and point to it with the pop val now in D
M=D // dump the value currently in the D register into the specified location
// no need to reset A, the base command will take care of that
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@36
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@R13
M=D // currently D contains the pop val but we need that register so dump D out to R13 which is unused
@THIS // point A at THIS
D=M // dump the base value into D
@6 // put the index in A
D=D+A // add the index and base and dump the result in D
@R14
M=D // dump the computed address to R14 which is unused
@R13
D=M // pull the pop val back out of R13 and dump in D
@R14
A=M // pull the computed address back out of R14 and point to it with the pop val now in D
M=D // dump the value currently in the D register into the specified location
// no need to reset A, the base command will take care of that
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@42
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@45
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@R13
M=D // currently D contains the pop val but we need that register so dump D out to R13 which is unused
@THAT // point A at THAT
D=M // dump the base value into D
@5 // put the index in A
D=D+A // add the index and base and dump the result in D
@R14
M=D // dump the computed address to R14 which is unused
@R13
D=M // pull the pop val back out of R13 and dump in D
@R14
A=M // pull the computed address back out of R14 and point to it with the pop val now in D
M=D // dump the value currently in the D register into the specified location
// no need to reset A, the base command will take care of that
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@R13
M=D // currently D contains the pop val but we need that register so dump D out to R13 which is unused
@THAT // point A at THAT
D=M // dump the base value into D
@2 // put the index in A
D=D+A // add the index and base and dump the result in D
@R14
M=D // dump the computed address to R14 which is unused
@R13
D=M // pull the pop val back out of R13 and dump in D
@R14
A=M // pull the computed address back out of R14 and point to it with the pop val now in D
M=D // dump the value currently in the D register into the specified location
// no need to reset A, the base command will take care of that
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@510
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@11 // point at the appropriate temp section
M=D // dump the popped off value into the appropriate pointer section
// no need to reset A, the base command will take care of that
D=0 // zero out the registers to prevent cross command contamination
A=0
@LCL // point A at LCL
D=M // dump the base value into D
@0 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THAT // point A at THAT
D=M // dump the base value into D
@5 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=D+M
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // point A at ARG
D=M // dump the base value into D
@1 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=M-D
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THIS // point A at THIS
D=M // dump the base value into D
@6 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THIS // point A at THIS
D=M // dump the base value into D
@6 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=D+M
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=M-D
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@11 // point at the appropriate temp section
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=D+M
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
