// SP=256
@256
D=A
@SP
M=D
// call Sys.init
@BOOTSTRAP // first load the return address value into D
D=A // push the return address value onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@LCL // first load the LCL base address value into D
D=M // push the LCL base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // first load the ARG base address value into D
D=M // push the ARG base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THIS // first load the THIS base address value into D
D=M // push the THIS base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THAT // first load the THAT base address value into D
D=M // push the THAT base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP
D=M
@0
D=D-A
@5
D=D-A // compute SP-n-5 and store in D
@ARG
M=D // store SP-n-5 in arg
@SP
D=M
@LCL
M=D // store the value at SP in LCL
@Sys.init
0;JMP
(BOOTSTRAP)
A=0
D=0 // clean up from function call just completed
(Class1.set)
A=0
D=0 // clear out the registers before we hit the body
@ARG // point A at ARG
D=M // dump the base value into D
@0 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@Class1.vm.0
M=D // dump the contents of D into the register
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // point A at ARG
D=M // dump the base value into D
@1 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@Class1.vm.1
M=D // dump the contents of D into the register
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@0
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// FRAME = LCL
@LCL
D=M
@R13
M=D
// RET = *(FRAME-5)
@5
D=D-A // compute FRAME-5 and store in D
A=D // load FRAME-5 into A
D=M // load *FRAME-5 into D
@R14
M=D // put *(FRAME-5) into RET
// *ARG = pop()
A=0
D=0 // clear out registers
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
// top of the stack is now in D
@ARG
A=M // point at *ARG
M=D // load the front of the stack into *ARG
// SP = ARG+1
@ARG
D=M+1 // compute ARG+1
@SP
M=D // load ARG+1 into SP
// THAT = *(FRAME-1)
@R13
D=M // put the address of FRAME in D
@1
D=D-A // compute FRAME-1 and store in D
A=D // load FRAME-1 into A
D=M // load *FRAME-1 into D
@THAT
M=D // put *FRAME-1 into THAT
// THIS = *(FRAME-2)
@R13
D=M // put the address of FRAME in D
@2
D=D-A // compute FRAME-2 and store in D
A=D // load FRAME-2 into A
D=M // load *FRAME-2 into D
@THIS
M=D // put *FRAME-2 into THIS
// ARG = *(FRAME-3)
@R13
D=M // put the address of FRAME in D
@3
D=D-A // compute FRAME-3 and store in D
A=D // load FRAME-3 into A
D=M // load *FRAME-3 into D
@ARG
M=D // put *FRAME-3 into ARG
// LCL = *(FRAME-4)
@R13
D=M // put the address of FRAME in D
@4
D=D-A // compute FRAME-4 and store in D
A=D // dumps FRAME-4 into A
D=M // loads *FRAME-4 (aka the return address) into D
@LCL
M=D // put *FRAME-4 into LCL
// goto RET
D=0 // clear out D
@R14
A=M
0;JMP
// you can't clean up the registers in this command, you have to clean up in the caller
(Class1.get)
A=0
D=0 // clear out the registers before we hit the body
// what needs to happen here is we need to point at the static sym
@Class1.vm.0
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// what needs to happen here is we need to point at the static sym
@Class1.vm.1
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=M-D
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// FRAME = LCL
@LCL
D=M
@R13
M=D
// RET = *(FRAME-5)
@5
D=D-A // compute FRAME-5 and store in D
A=D // load FRAME-5 into A
D=M // load *FRAME-5 into D
@R14
M=D // put *(FRAME-5) into RET
// *ARG = pop()
A=0
D=0 // clear out registers
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
// top of the stack is now in D
@ARG
A=M // point at *ARG
M=D // load the front of the stack into *ARG
// SP = ARG+1
@ARG
D=M+1 // compute ARG+1
@SP
M=D // load ARG+1 into SP
// THAT = *(FRAME-1)
@R13
D=M // put the address of FRAME in D
@1
D=D-A // compute FRAME-1 and store in D
A=D // load FRAME-1 into A
D=M // load *FRAME-1 into D
@THAT
M=D // put *FRAME-1 into THAT
// THIS = *(FRAME-2)
@R13
D=M // put the address of FRAME in D
@2
D=D-A // compute FRAME-2 and store in D
A=D // load FRAME-2 into A
D=M // load *FRAME-2 into D
@THIS
M=D // put *FRAME-2 into THIS
// ARG = *(FRAME-3)
@R13
D=M // put the address of FRAME in D
@3
D=D-A // compute FRAME-3 and store in D
A=D // load FRAME-3 into A
D=M // load *FRAME-3 into D
@ARG
M=D // put *FRAME-3 into ARG
// LCL = *(FRAME-4)
@R13
D=M // put the address of FRAME in D
@4
D=D-A // compute FRAME-4 and store in D
A=D // dumps FRAME-4 into A
D=M // loads *FRAME-4 (aka the return address) into D
@LCL
M=D // put *FRAME-4 into LCL
// goto RET
D=0 // clear out D
@R14
A=M
0;JMP
// you can't clean up the registers in this command, you have to clean up in the caller
(Class2.set)
A=0
D=0 // clear out the registers before we hit the body
@ARG // point A at ARG
D=M // dump the base value into D
@0 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@Class2.vm.0
M=D // dump the contents of D into the register
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // point A at ARG
D=M // dump the base value into D
@1 // put the index in A
A=D+A // add the index and base and point at the result
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@Class2.vm.1
M=D // dump the contents of D into the register
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@0
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// FRAME = LCL
@LCL
D=M
@R13
M=D
// RET = *(FRAME-5)
@5
D=D-A // compute FRAME-5 and store in D
A=D // load FRAME-5 into A
D=M // load *FRAME-5 into D
@R14
M=D // put *(FRAME-5) into RET
// *ARG = pop()
A=0
D=0 // clear out registers
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
// top of the stack is now in D
@ARG
A=M // point at *ARG
M=D // load the front of the stack into *ARG
// SP = ARG+1
@ARG
D=M+1 // compute ARG+1
@SP
M=D // load ARG+1 into SP
// THAT = *(FRAME-1)
@R13
D=M // put the address of FRAME in D
@1
D=D-A // compute FRAME-1 and store in D
A=D // load FRAME-1 into A
D=M // load *FRAME-1 into D
@THAT
M=D // put *FRAME-1 into THAT
// THIS = *(FRAME-2)
@R13
D=M // put the address of FRAME in D
@2
D=D-A // compute FRAME-2 and store in D
A=D // load FRAME-2 into A
D=M // load *FRAME-2 into D
@THIS
M=D // put *FRAME-2 into THIS
// ARG = *(FRAME-3)
@R13
D=M // put the address of FRAME in D
@3
D=D-A // compute FRAME-3 and store in D
A=D // load FRAME-3 into A
D=M // load *FRAME-3 into D
@ARG
M=D // put *FRAME-3 into ARG
// LCL = *(FRAME-4)
@R13
D=M // put the address of FRAME in D
@4
D=D-A // compute FRAME-4 and store in D
A=D // dumps FRAME-4 into A
D=M // loads *FRAME-4 (aka the return address) into D
@LCL
M=D // put *FRAME-4 into LCL
// goto RET
D=0 // clear out D
@R14
A=M
0;JMP
// you can't clean up the registers in this command, you have to clean up in the caller
(Class2.get)
A=0
D=0 // clear out the registers before we hit the body
// what needs to happen here is we need to point at the static sym
@Class2.vm.0
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// what needs to happen here is we need to point at the static sym
@Class2.vm.1
D=M // grab the value out of that register and store it in D
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP // point A to address 0
A=M-1 // derive address of first element from address of SP and load it into A
D=M // get the val of first element and store in D
A=A-1 // reset A to next element in stack
M=M-D
A=A+1 // reset A to first element in the stack
M=0 // zero out the top position in the stack
@SP // repoint A to 0
M=M-1 // reset the address at 0 to the new top of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// FRAME = LCL
@LCL
D=M
@R13
M=D
// RET = *(FRAME-5)
@5
D=D-A // compute FRAME-5 and store in D
A=D // load FRAME-5 into A
D=M // load *FRAME-5 into D
@R14
M=D // put *(FRAME-5) into RET
// *ARG = pop()
A=0
D=0 // clear out registers
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
// top of the stack is now in D
@ARG
A=M // point at *ARG
M=D // load the front of the stack into *ARG
// SP = ARG+1
@ARG
D=M+1 // compute ARG+1
@SP
M=D // load ARG+1 into SP
// THAT = *(FRAME-1)
@R13
D=M // put the address of FRAME in D
@1
D=D-A // compute FRAME-1 and store in D
A=D // load FRAME-1 into A
D=M // load *FRAME-1 into D
@THAT
M=D // put *FRAME-1 into THAT
// THIS = *(FRAME-2)
@R13
D=M // put the address of FRAME in D
@2
D=D-A // compute FRAME-2 and store in D
A=D // load FRAME-2 into A
D=M // load *FRAME-2 into D
@THIS
M=D // put *FRAME-2 into THIS
// ARG = *(FRAME-3)
@R13
D=M // put the address of FRAME in D
@3
D=D-A // compute FRAME-3 and store in D
A=D // load FRAME-3 into A
D=M // load *FRAME-3 into D
@ARG
M=D // put *FRAME-3 into ARG
// LCL = *(FRAME-4)
@R13
D=M // put the address of FRAME in D
@4
D=D-A // compute FRAME-4 and store in D
A=D // dumps FRAME-4 into A
D=M // loads *FRAME-4 (aka the return address) into D
@LCL
M=D // put *FRAME-4 into LCL
// goto RET
D=0 // clear out D
@R14
A=M
0;JMP
// you can't clean up the registers in this command, you have to clean up in the caller
(Sys.init)
A=0
D=0 // clear out the registers before we hit the body
// this command just loads whatever constant you want into the D register
@6
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@8
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SYMb // first load the return address value into D
D=A // push the return address value onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@LCL // first load the LCL base address value into D
D=M // push the LCL base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // first load the ARG base address value into D
D=M // push the ARG base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THIS // first load the THIS base address value into D
D=M // push the THIS base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THAT // first load the THAT base address value into D
D=M // push the THAT base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP
D=M
@2
D=D-A
@5
D=D-A // compute SP-n-5 and store in D
@ARG
M=D // store SP-n-5 in arg
@SP
D=M
@LCL
M=D // store the value at SP in LCL
@Class1.set
0;JMP
(SYMb)
A=0
D=0 // clean up from function call just completed
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@5 // point at the appropriate temp section
M=D // dump the popped off value into the appropriate pointer section
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@23
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
// this command just loads whatever constant you want into the D register
@15
D=A
// no need to reset A, the base command will take care of that
// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SYMc // first load the return address value into D
D=A // push the return address value onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@LCL // first load the LCL base address value into D
D=M // push the LCL base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // first load the ARG base address value into D
D=M // push the ARG base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THIS // first load the THIS base address value into D
D=M // push the THIS base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THAT // first load the THAT base address value into D
D=M // push the THAT base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP
D=M
@2
D=D-A
@5
D=D-A // compute SP-n-5 and store in D
@ARG
M=D // store SP-n-5 in arg
@SP
D=M
@LCL
M=D // store the value at SP in LCL
@Class2.set
0;JMP
(SYMc)
A=0
D=0 // clean up from function call just completed
@SP // point A at the location of the SP address
A=M-1
D=M
M=0
@SP
M=M-1
A=0 // reset the A register but leave the popped off value
@5 // point at the appropriate temp section
M=D // dump the popped off value into the appropriate pointer section
D=0 // zero out the registers to prevent cross command contamination
A=0
@SYMd // first load the return address value into D
D=A // push the return address value onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@LCL // first load the LCL base address value into D
D=M // push the LCL base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // first load the ARG base address value into D
D=M // push the ARG base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THIS // first load the THIS base address value into D
D=M // push the THIS base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THAT // first load the THAT base address value into D
D=M // push the THAT base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP
D=M
@0
D=D-A
@5
D=D-A // compute SP-n-5 and store in D
@ARG
M=D // store SP-n-5 in arg
@SP
D=M
@LCL
M=D // store the value at SP in LCL
@Class1.get
0;JMP
(SYMd)
A=0
D=0 // clean up from function call just completed
@SYMe // first load the return address value into D
D=A // push the return address value onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@LCL // first load the LCL base address value into D
D=M // push the LCL base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@ARG // first load the ARG base address value into D
D=M // push the ARG base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THIS // first load the THIS base address value into D
D=M // push the THIS base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@THAT // first load the THAT base address value into D
D=M // push the THAT base address onto the stack

// assume the the value we want to push is already in the D register
@SP // point to the value in SP
A=M
M=D // dump the value in D into the front of the stack
@SP // look back at SP
M=M+1 // increment it up to reflect new state of the stack
D=0 // zero out the registers to prevent cross command contamination
A=0
@SP
D=M
@0
D=D-A
@5
D=D-A // compute SP-n-5 and store in D
@ARG
M=D // store SP-n-5 in arg
@SP
D=M
@LCL
M=D // store the value at SP in LCL
@Class2.get
0;JMP
(SYMe)
A=0
D=0 // clean up from function call just completed
(WHILE)
A=0
D=0 // you need to clean up the registers here because you won't be able to clean up in the jump commands
@WHILE
0;JMP
// we can't clean up our registers here, we have to do it in the label command
