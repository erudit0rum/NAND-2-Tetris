# packages
import re
import yaml
import os

# classes
from parser import Parser
from code_writer import Code

# input and output files
bootStrapThis = False

#input_file_path = '../MemoryAccess/PointerTest/PointerTest.vm'
#output_file_path = 'PointerTest.asm'

#input_file_path = '../MemoryAccess/BasicTest/BasicTest.vm'
#output_file_path = 'BasicTest.asm'

#input_file_path = '../MemoryAccess/StaticTest/StaticTest.vm'
#output_file_path = 'StaticTest.asm'

#input_file_path = '../StackArithmetic/SimpleAdd/SimpleAdd.vm'
#output_file_path = 'SimpleAdd.asm'

#input_file_path = '../StackArithmetic/StackTest/StackTest.vm'
#output_file_path = 'StackTest.asm'

#input_file_path = '../ProgramFlow/BasicLoop'
#output_file_path = 'BasicLoop.asm'

#input_file_path = '../ProgramFlow/FibonacciSeries'
#output_file_path = 'FibonacciSeries.asm'

#input_file_path = '../FunctionCalls/SimpleFunction'
#output_file_path = 'SimpleFunction.asm'

#input_file_path = '../FunctionCalls/NestedCall'
#output_file_path = 'NestedCall.asm'

input_file_path = '../FunctionCalls/FibonacciElement'
output_file_path = 'FibonacciElement.asm'
bootStrapThis = True

#input_file_path = '../FunctionCalls/StaticsTest'
#output_file_path = 'StaticsTest.asm'
#bootStrapThis = True

# config files
init_command_path = 'init_commands.yaml'
stack_command_path = 'stack_commands.yaml'
push_command_path = 'push_commands.yaml'
pop_command_path = 'pop_commands.yaml'
flow_command_path = 'flow_commands.yaml'
function_command_path = 'function_commands.yaml'


# instantiate the classes and pass them their dependencies
prs = Parser(input_file_path, re, os)
cdwrt = Code(
    output_file_path, init_command_path, stack_command_path,
    push_command_path, pop_command_path, flow_command_path,
    function_command_path, yaml
)

def sub_routine(bootstrap):
    cdwrt.write_init() if bootstrap else False
    while prs.has_more_commands():
        com_type = prs.command_type()
        base_command = prs.current_base_command()
        # why am i passing in these base commands here again?
        # the code writer should be able to pull those itself right off the
        # code writer object itself
        if com_type == 'C_POP' or com_type == 'C_PUSH':
            arg1 = prs.arg1()
            arg2 = prs.arg2()
            cdwrt.write_push_pop(base_command, arg1, arg2)
        elif com_type == 'C_ARITHMETIC':
            cdwrt.write_arithmetic(base_command)
        elif com_type == 'C_LABEL':
            label = prs.arg1()
            cdwrt.write_label(label)
        elif com_type == 'C_GOTO':
            label = prs.arg1()
            cdwrt.write_go_to(label)
        elif com_type == 'C_IF':
            label = prs.arg1()
            cdwrt.write_if(label)
        elif com_type == 'C_FUNCTION':
            functionName = prs.arg1()
            numLocals = prs.arg2()
            cdwrt.write_function(functionName, numLocals)
        elif com_type == 'C_RETURN':
            cdwrt.write_return()
        elif com_type == 'C_CALL':
            functionName = prs.arg1()
            numArgs = prs.arg2()
            cdwrt.write_call(functionName, numArgs)
        elif com_type == 'newfile':
            filename = prs.arg1()
            cdwrt.set_file_name(filename)
        else:
            print 'not equipped to handle this'
        prs.advance()
    cdwrt.close()

def main(bootstrap):
    sub_routine(bootstrap)

main(bootStrapThis)
