// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.
@24575
D=A
@END
M=D // END = 24575

@16384
D=A
@CURRENT
M=D // CURRENT = 16384

(LOOP)
	@KBD
	D=M
	@ELSEONE
	D;JEQ
	@CURRENT
	D=M
	A=D
	M=-1
	@ENDELSEONE
	0;JMP
	(ELSEONE)
		@CURRENT
		D=M
		A=D
		M=0
	(ENDELSEONE)
	@CURRENT
	D=M
	@END
	D=D-M
	@ELSETWO
	D;JNE
	@16384
	D=A
	@CURRENT
	M=D
	@ENDELSETWO
	0;JMP
	(ELSETWO)
		@CURRENT
		M=M+1
	(ENDELSETWO)
	@LOOP
	0;JMP
