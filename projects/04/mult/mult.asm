// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.

@0 // This sets R2 to 0
D=A
@R2
M=D

@R0 // This jumps to the end if R0 equals 0
D=M
@DONE
D;JEQ

@R1 // This jumps to the end if R1 equals 0
D=M
@DONE
D;JEQ

(LOOP) // This increments R2 by R0 and then
@R0 // decrements R1 until R1 equals 0
D=M
@R2
M=D+M
@R1
M=M-1
D=M
@LOOP
D;JGT

(DONE)
@DONE
0;JMP
