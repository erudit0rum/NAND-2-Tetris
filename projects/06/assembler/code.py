class Code:
    def __init__(self, yaml_obj, dest_config_path, comp_config_path, jump_config_path):
        print 'initializing code module'
        self.__dest_lookup = yaml_obj.safe_load(open(dest_config_path))
        self.__comp_lookup = yaml_obj.safe_load(open(comp_config_path))
        self.__jump_lookup = yaml_obj.safe_load(open(jump_config_path))

    def __look_up(self, symbol, table):
        if symbol in getattr(self, table):
            return getattr(self, table)[symbol]
        else:
            raise Exception('symbol not present')

    def dest(self, symbol):
        return self.__look_up(symbol, '_Code__dest_lookup')

    def comp(self, symbol):
        return self.__look_up(symbol, '_Code__comp_lookup')

    def jump(self, symbol):
        return self.__look_up(symbol, '_Code__jump_lookup')
