class SymbolTable:
    def __init__(self, yaml_obj, predefined_symbols_path):
        print 'intializing symboltable module'
        self.symbols = yaml_obj.safe_load(open(predefined_symbols_path))

    def add_entry(self, symbol, address):
        self.symbols[symbol] = address

    def contains(self, symbol):
        return symbol in self.symbols.keys()

    def get_address(self, symbol):
        return self.symbols[symbol]
