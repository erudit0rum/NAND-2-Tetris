# packages
import re
import yaml

# classes
from code import Code
from parser import Parser
from symboltable import SymbolTable

# input and output files
input_file_path = 'max/MaxL.asm'
output_file_path = 'MaxL_output.hack'

# configuration files
dest = 'assembler/dest_lookup.yaml'
comp = 'assembler/comp_lookup.yaml'
jump = 'assembler/jump_lookup.yaml'
predefined_symbols = 'assembler/predefined_symbols.yaml'

# instantiate the classes and pass them their dependencies
prs = Parser(input_file_path, re)
cd = Code(yaml, dest, comp, jump)
symtbl = SymbolTable(yaml, predefined_symbols)

def handle_A_command():
    print 'entering handle A command'
    a_address = prs.current_command().split('@')[1]
    if symtbl.contains(a_address):
        return '0' + str(symtbl.get_address(a_address))
    else:
        return str(bin(int(a_address))).replace('0b', '').rjust(16, '0')

def handle_C_command():
    print 'entering handle C command'
    return \
    '111' + \
    cd.comp(prs.comp()) + \
    cd.dest(prs.dest()) + \
    cd.jump(prs.jump())

def handle_L_command():
    print 'entering handle L command'
    if not symtbl.contains(prs.symbol()):
        symtbl.add_entry(prs.symbol(), \
        bin(prs.current_line()).replace('0b', '').rjust(15, '0'))

def pre_assemble():
    print 'entering preassemble'

    print 'adding symbols to symbol table'
    while prs.has_more_commands():
        if prs.command_type() == 'L_COMMAND':
            handle_L_command()
            prs.delete_current_line()
            continue
        prs.advance()
    prs.reset_current_line()

    print 'adding variables to symbol table'
    new_var = 16
    while prs.has_more_commands():
        if prs.command_type() == 'A_COMMAND':
            a_command = prs.current_command().split('@')[1]
            try:
                int(a_command)
                can_be_num = True
            except:
                can_be_num = False
            if not (symtbl.contains(a_command) or can_be_num):
                loc = bin(new_var).replace('0b', '').rjust(15, '0')
                symtbl.add_entry(a_command, loc)
                new_var += 1
        prs.advance()
    prs.reset_current_line()

    print 'exiting preassemble'

def assemble():
    print 'entering assemble'

    print 'constructing binary commands'
    bin_array = []
    while prs.has_more_commands():
        command_type = prs.command_type()
        if command_type == 'A_COMMAND':
            bin_array.append(handle_A_command())
        else:
            bin_array.append(handle_C_command())
        prs.advance()
    prs.reset_current_line()

    print 'writing commands to ' + output_file_path
    hack_output = open(output_file_path, 'w+')
    for line in bin_array:
        hack_output.write(line)
        hack_output.write('\n')
    hack_output.close()

    print 'exiting asemble'

def main():
    print 'entering main'
    pre_assemble()
    assemble()
    print 'exiting main'

main()
