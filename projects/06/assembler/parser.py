class Parser:
    def __init__(self, file_name, regex_obj):
        print 'initializing parser module'

        print 'attaching the regex object to the parser'
        self.__regex = regex_obj

        print 'opening file'
        with open(file_name) as f:
            self.__program = f.readlines()
        f.close();

        print 'setting current line to 0'
        self.__current_line = 0

        print 'stripping out comments, white space, and empty lines'
        while self.__current_line < len(self.__program):

            # if line is blank or a comment then strip it out
            comment_loc = self.__program[self.__current_line].find('//')
            if comment_loc == 0 or \
            self.__program[self.__current_line] == '' or \
            self.__program[self.__current_line].isspace():
                del self.__program[self.__current_line]
                continue

            if comment_loc > 0:
                self.__program[self.__current_line] = \
                self.__program[self.__current_line].split('//')[0]

            self.__program[self.__current_line] = \
            self.__program[self.__current_line].replace('\n', '').replace('\r', '').replace(' ', '')

            self.__current_line += 1

        print 'resetting the current line to 0'
        self.__current_line = 0

    def has_more_commands(self):
        return self.__current_line <= len(self.__program) - 1

    def advance(self):
        self.__current_line += 1

    def command_type(self):
        if self.__regex.search('\@.*', self.__program[self.__current_line]):
            return 'A_COMMAND'
        elif self.__regex.search('(?<!.)[(].*[)](?!.)', self.__program[self.__current_line]):
            return 'L_COMMAND'
        else:
            return 'C_COMMAND'

    def symbol(self):
        if self.command_type() == 'L_COMMAND':
            return self.__program[self.__current_line].split('(')[1].split(')')[0]
        else:
            raise Exception()

    def dest(self):
        if self.command_type() == 'C_COMMAND':
            return 'null' if \
            self.__program[self.__current_line].find('=') == -1 else \
            self.__program[self.__current_line].split('=')[0]
        else:
            raise Exception()

    def comp(self):
        if self.command_type() == 'C_COMMAND':
            comp = self.__program[self.__current_line]

            if comp.find('=') != -1:
                comp = comp.split('=')[1]

            if comp.find(';') != -1:
                comp = comp.split(';')[0]

            return comp
        else:
            raise Exception()

    def jump(self):
        if self.command_type() == 'C_COMMAND':
            return 'null' if \
            self.__program[self.__current_line].find(';') == -1 else \
            self.__program[self.__current_line].split(';')[1]
        else:
            raise Exception()

    def current_command(self):
        return self.__program[self.__current_line]

    def current_line(self):
        return self.__current_line

    def reset_current_line(self):
        self.__current_line = 0

    def delete_current_line(self):
        del self.__program[self.__current_line]
