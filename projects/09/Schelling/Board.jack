/**
Board Class

Constructor Methods:

-init
  @purpose /
    standard constructor method, creates an instance of the Board class
  @param / /
  @param / /
  @param / /
  @return / an instance of the Board class
  @sideEffect / none

Mutator Methods:

-newBoard
  @purpose /
    this method will spin up a random new board with a specified number of black
    and white squares, the rest of the squares will be gray
  @param / int / x / number of columns to have on the board
  @param / int / y / number of rows to have on the board
  @param / int / black / number of black squares on the board
  @param / int / white / number of white squares on the board
  @return / none
  @sideEffect / initializes Board with specified dimensions and squares

-destroyBoard
  @purpose /
    this method will destroy the currently existing board if there is one
  @return / none
  @sideEffect / destroys the current board if there is one

-moveBoard
  @purpose /
    this method will move the board ahead a specified number of 'turns' in accord with the rules laid out in the docs
  @param / int / numTurns / number of turns to move the board ahead by
  @return / none
  @sideEffect / 'increment' the board by one move

Selector Methods:

-getChanges
  @purpose /
    this method returns all the changes made to the board in the last run of moveBoard, i.e. it will return a list of every
    square that needs to be updated and what it needs to be updated to
  @return / array / an array of 3 membered arrays, an x int a y int and a string indicating whether the square should be white, black, neutral
  @sideEffect / none

-getSquare
  @purpose /
    this method allows external logic to peek at a particular square on the board
  @return / string / a string representing whether the desired square is white, black, or neutral
  @sideEffect / none

-getBoard
  @purpose /
    this method will basically just allow you to bypass running getSquare a whole bunch of times and will return the whole board
  @return / array / an array of 3 membered arrays, an x int a y int and a string indicating whether the square should be white, black, neutral,
    unlike getChanges, this one should be for the whole board.
  @sideEffect / none
*/

/**
Further developer notes:

- For the moveBoard and the getChanges methods, here is how the interplay needs to work:
First there needs to be an array field called a change, this change field needs to always
hold a matrix recording all the changes made in the last run of the moveBoard method (initially
of course it will hold an empty matrix. When moveboard is called, it will wipe the changes field
and then record each new change back into the field as it goes. Then, the getChanges method can
deliver those changes up to you (perhaps after passing a flag about whether or not to flatten
them? add that in as a stretch goal)

- Thinking through the implications of not being able to pull length off of Arrays I'm thinking that
it may be a good idea to move over to linked lists as the better way to deal with looping through chains of
data, this would make it real easy to detect when the data has been fully looped through, moreover, the majority
of the code has been written, all I would need to do is snarf the list code and change the data type for other types
of data, then you just use a while loop to cycle through the linked list and terminate when the current link is equal to
null, moreover, you could use data type array to get all the flexibility you want out of arrays to work here.

- after considering either wrapping arrays to provide them with a length method or using linked lists I think that the
simplest solution is to do neither but rather to do something like this: have a method thatcreates arrays that are a
certain length and prefilled with nulls, then, you can fill them up with other values, when this is done you have an array
that you can cycle through without knowing how long it is and still be able to tell when it is done because you can monitor
for the null value (or perhaps some unique string that we set by convention, ) in this way you can basically have type
agnostic arrays that have a sort of length method baked into them, performance will suck but it will work, right now that is
the main concern

*/

class Board {
   field int numColumns, numRows, numBlack, numWhite;
   field boolean hasBoard;
   field Array boardMatrix, changeMatrix;

   constructor Board new() {
      let numColumns = 0
      let numRows = 0
      let numBlack = 0
      let numWhite = 0
      let hasBoard = false
      let boardMatrix = Array.new(0)
      let changeMatrix = Array.new(0)
      return this;
   }

   method void newBoard(int x, int y, String black, String white) {
      let numColumns = x
      let numRows = y
      let numBlack = black
      let numWhite = white
      let hasBoard = true
      let boardMatrix = Array.new(x)
      let i = 1
      while (~(i > x)) {
          boardMatrix[i] = Array.new(y)
          let i = i + 1
      }
      let changeMatrix = Array.new(0)
      return;
   }

   method void destroyBoard() {
      let numColumns = 0
      let numRows = 0
      let numBlack = 0
      let numWhite = 0
      let hasBoard = false
      let boardMatrix = Array.new(0)
      let changeMatrix = Array.new(0)
      return;
   }

   method void moveBoard() {
      var Array unsatisfied;
      var Array open;
      let unsatisfied = getUnsatisfied();
      let open = getOpen();
      let unsatisfied = randomize(unsatisfied);
      let open = randomize(open);
      return;
   }

   method Array getChanges() {
      return changeMatrix;
   }

   method Array getBoard() {
      return boardMatrix;
   }

   method String getSquare(int x, int y) {
      return boardMatrix[x][y];
   }

// private helper methods
   method Array randomize(Array initial) {
      var Array random;
      let Arra
   }


///////////


   /** Accessors. */
   method int getNumerator() { return numerator; }
   method int getDenominator() { return denominator; }  

   /** Returns the sum of this fraction and the other one. */
   method Fraction plus(Fraction other) {
      var int sum;
      let sum = (numerator * other.getDenominator()) + (other.getNumerator() * denominator);
      return Fraction.new(sum, denominator * other.getDenominator());
   }

   // More fraction-related methods (minus, times, div, etc.) can be added here.

   /** Disposes this fraction. */
   method void dispose() {
      do Memory.deAlloc(this);  // uses an OS routine to recycle the memory held by the object
      return;
   }

   /** Prints this fraction in the format x/y. */
   method void print() {
      do Output.printInt(numerator);
      do Output.printString("/");
      do Output.printInt(denominator);
      return;
   }

   // Computes the greatest common divisor of the given integers.
   function int gcd(int a, int b) {
      var int r;
      while (~(b = 0)) {             // applies Euclid's algorithm
         let r = a - (b * (a / b));  // r = remainder of the integer division a/b
         let a = b; let b = r;
      }
      return a;
   }
}
