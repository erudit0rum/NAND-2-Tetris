// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/b/RAM16K.hdl

/**
 * Memory of 16K registers, each 16 bit-wide. Out holds the value
 * stored at the memory location specified by address. If load==1, then 
 * the in value is loaded into the memory location specified by address 
 * (the loaded value will be emitted to out from the next time step onward).
 */

CHIP RAM16K {
    IN in[16], load, address[14];
    OUT out[16];

    PARTS:
    DMux8Way(in=load,
    sel[2]=address[13],
    sel[1]=address[12],
    sel[0]=address[11],
    a=loadA, b=loadB, c=loadC, d=loadD,
    e=loadE, f=loadF, g=loadG, h=loadH);
    DMux8Way16(in=in,
    sel[2]=address[13],
    sel[1]=address[12],
    sel[0]=address[11],
    a=inA, b=inB, c=inC, d=inD,
    e=inE, f=inF, g=inG, h=inH);
    RAM4K(
    in=inA, out=outA, load=loadA,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register A
    RAM4K(
    in=inB, out=outB, load=loadB,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register B
    RAM4K(
    in=inC, out=outC, load=loadC,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register C
    RAM4K(
    in=inD, out=outD, load=loadD,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register D
    RAM4K(
    in=inE, out=outE, load=loadE,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register E
    RAM4K(
    in=inF, out=outF, load=loadF,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register F
    RAM4K(
    in=inG, out=outG, load=loadG,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register G
    RAM4K(
    in=inH, out=outH, load=loadH,
    address[11]=address[11],
    address[10]=address[10],
    address[9]=address[9],
    address[8]=address[8],
    address[7]=address[7],
    address[6]=address[6],
    address[5]=address[5],
    address[4]=address[4],
    address[3]=address[3],
    address[2]=address[2],
    address[1]=address[1],
    address[0]=address[0]
    ); // register H
    Mux8Way16(out=out,
    sel[2]=address[13],
    sel[1]=address[12],
    sel[0]=address[11],
    a=outA, b=outB, c=outC, d=outD,
    e=outE, f=outF, g=outG, h=outH);

}
